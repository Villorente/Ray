package com.smsgt.ray;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Step3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step3);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView text = (TextView) findViewById(R.id.text);
        text.setTypeface(nunito);
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(nunitoBold);

        LinearLayout step3View = (LinearLayout) findViewById(R.id.step3_view);
        step3View.setOnTouchListener(new OnSwipeTouchListener(this.getApplicationContext()){
            @Override
            public void onSwipeLeft() {
                Intent intent = new Intent(Step3.this, Step4.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                finish();
            }
            @Override
            public void onSwipeRight() {
                Intent intent = new Intent(Step3.this, Step2.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(Step3.this, Step2.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        finish();
    }
}
