package com.smsgt.ray;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ForgotPassword extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");

        TextView forgotPasswordLabel = (TextView) findViewById(R.id.forgot_password);
        forgotPasswordLabel.setTypeface(nunitoBold);

        TextView forgotPasswordText = (TextView) findViewById(R.id.forgot_password_text);
        forgotPasswordText.setTypeface(nunito);

        TextView emailLabel = (TextView) findViewById(R.id.email_label);
        emailLabel.setTypeface(nunito);

        EditText emailValue = (EditText) findViewById(R.id.email_value);
        emailValue.setTypeface(nunito);

        Button sendPasswordButton = (Button) findViewById(R.id.send_password);
        sendPasswordButton.setTypeface(nunito);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ForgotPassword.this, Login.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        finish();
    }
}
