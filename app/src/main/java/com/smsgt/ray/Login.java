package com.smsgt.ray;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login extends AppCompatActivity {
    private String ip = "116.50.170.243";
    private String port = "50262";
    public String token = null;
    public String mesh_id = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        final Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");

        TextView welcome = (TextView) findViewById(R.id.welcome);
        welcome.setTypeface(nunitoBold);

        final TextView emailLabel = (TextView) findViewById(R.id.email_label);
        emailLabel.setTypeface(nunito);

        final EditText emailValue = (EditText) findViewById(R.id.email_value);
        emailValue.setTypeface(nunito);

        final TextView passwordLabel = (TextView) findViewById(R.id.password_label);
        passwordLabel.setTypeface(nunito);

        final EditText passwordValue = (EditText) findViewById(R.id.password_value);
        passwordValue.setTypeface(nunito);

        Button loginButton = (Button) findViewById(R.id.login);
        loginButton.setTypeface(nunito);

        TextView alreadyAMember = (TextView) findViewById(R.id.new_user);
        alreadyAMember.setTypeface(nunito);

        TextView login = (TextView) findViewById(R.id.sign_up);
        login.setTypeface(nunito);

        TextView google = (TextView) findViewById(R.id.google);
        google.setTypeface(nunito);

        TextView facebook = (TextView) findViewById(R.id.facebook);
        facebook.setTypeface(nunito);

        final ImageView passwordShowOrHide = (ImageView) findViewById(R.id.show_or_hide);
        final View passwordLine = (View) findViewById(R.id.password_line);
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        final ViewGroup.LayoutParams layoutParams = passwordLine.getLayoutParams();
        passwordShowOrHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutParams.height = (int) (2 * scale + 0.5f);
                passwordLine.setLayoutParams(layoutParams);
                int passwordLength = passwordValue.getText().length();
                if(passwordValue.getInputType() == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)) {
                    passwordShowOrHide.setImageResource(R.drawable.ic_hide_gray_16dp);
                    passwordValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                    passwordValue.requestFocus();
                    passwordValue.setSelection(passwordLength, passwordLength);
                } else {
                    passwordShowOrHide.setImageResource(R.drawable.ic_show_gray_16dp);
                    passwordValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordValue.requestFocus();
                    passwordValue.setSelection(passwordLength, passwordLength);
                }
                passwordValue.setTypeface(nunito);
            }
        });
        passwordValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    layoutParams.height = (int) (2 * scale + 0.5f);
                    passwordLine.setLayoutParams(layoutParams);
                    passwordLine.setBackgroundColor(Color.parseColor("#AB47BC"));
                } else {
                    layoutParams.height = (int) (1 * scale + 0.5f);
                    passwordLine.setLayoutParams(layoutParams);
                    passwordLine.setBackgroundColor(Color.parseColor("#7F7F99"));
                    passwordLabel.setTextColor(Color.parseColor("#7F7F99"));
                }
            }
        });
        emailValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    emailValue.getBackground().setColorFilter(Color.parseColor("#AB47BC"), PorterDuff.Mode.SRC_IN);
                } else {
                    emailLabel.setTextColor(Color.parseColor("#7F7F99"));
                    emailValue.getBackground().setColorFilter(Color.parseColor("#7F7F99"), PorterDuff.Mode.SRC_IN);
                }
            }
        });


    }

    @Override
    public void onBackPressed () {
        Intent intent = new Intent(Login.this, Register.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        finish();
    }

    public void goToRegister(View view) {
        Intent intent = new Intent(Login.this, Register.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        finish();
    }

    public void goToForgotPassword(View view) {
        Intent intent = new Intent(Login.this, ForgotPassword.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
        finish();
    }

    public static boolean isEmailValid(String emailString) {
        String expression = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(emailString);
        return matcher.matches();
    }

    public void loginUser(View view) throws JSONException {
        LinearLayout loginLayout = (LinearLayout) findViewById(R.id.login_layout);
        TextView emailLabel = (TextView) findViewById(R.id.email_label);
        EditText emailValue = (EditText) findViewById(R.id.email_value);
        String emailString = emailValue.getText().toString();
        TextView passwordLabel = (TextView) findViewById(R.id.password_label);
        EditText passwordValue = (EditText) findViewById(R.id.password_value);
        View passwordLine = (View) findViewById(R.id.password_line);
        String passwordString = passwordValue.getText().toString();
        if (emailString.isEmpty()) {
            Snackbar snackbar = Snackbar.make(loginLayout, "Please enter email", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            emailValue.requestFocus();
            emailValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            emailLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else if (!(isEmailValid(emailString))) {
            Snackbar snackbar = Snackbar.make(loginLayout, "Please enter a valid email", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            emailValue.requestFocus();
            emailValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            emailLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else if (passwordString.isEmpty()) {
            Snackbar snackbar = Snackbar.make(loginLayout, "Please enter password", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            passwordValue.requestFocus();
            passwordLine.setBackgroundColor(Color.parseColor("#F04F6A"));
            passwordLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else {
            String urlString = "http://"+ip+":"+port+"/cake3/rd_cake/dashboard/authenticate.json";
            String responseString = null;

            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put("username", emailString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                jsonData.put("password", passwordString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonData.toString();
            Log.d("JSONSTRING", jsonString);
            HttpPostRequest httpPostRequest = new HttpPostRequest();
            try {
                responseString = httpPostRequest.execute(urlString, jsonString).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            if ((responseString != null) && (!responseString.isEmpty())) {
                Log.d("RESPONSE", responseString);
                JSONObject object = new JSONObject(responseString);
                if (object != null) {
                    Boolean success = object.getBoolean("success");
                    Log.d("SUCCESS", success.toString());
                    if (success == false) {
                        Snackbar snackbar = Snackbar.make(loginLayout, "Email or password is incorrect", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
                        emailValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
                        emailLabel.setTextColor(Color.parseColor("#F04F6A"));
                        passwordLine.setBackgroundColor(Color.parseColor("#F04F6A"));
                        passwordLabel.setTextColor(Color.parseColor("#F04F6A"));
                        snackbar.show();
                    } else {
                        JSONObject data = object.getJSONObject("data");
                        token = data.getString("token");
                        Log.d("TOKEN", token);
                        String urlStringToGetMeshId = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/index.json?token="+token;
                        String getMeshIdResponse = null;
                        HttpGetRequest getMeshIdRequest = new HttpGetRequest();
                        try {
                            getMeshIdResponse = getMeshIdRequest.execute(urlStringToGetMeshId).get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        Log.d("RESPONSE", getMeshIdResponse);
                        JSONObject meshIdResponseObject = new JSONObject(getMeshIdResponse);
                        Boolean getMeshIdSuccess = meshIdResponseObject.getBoolean("success");
                        if (getMeshIdSuccess) {
                            JSONArray items = meshIdResponseObject.getJSONArray("items");
                            if (items.length() == 0) {
                                SharedPreferences userSharedPreferences = getSharedPreferences("USER", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = userSharedPreferences.edit();
                                editor.putString("token", token);
                                editor.apply();
                                Intent intent = new Intent(Login.this, NameNetwork.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                                finish();
                            } else {
                                JSONObject item = items.getJSONObject(0);
                                mesh_id = item.getString("id");
                                Log.d("MESH ID", mesh_id);
                                Intent intent = new Intent(this.getApplication(), Dashboard.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                                //
                                SharedPreferences userSharedPreferences = getSharedPreferences("USER", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = userSharedPreferences.edit();
                                editor.putString("mesh_id", mesh_id);
                                editor.putString("token", token);
                                editor.apply();
                                //

                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Snackbar snackbar = Snackbar.make(loginLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                            snackbar.setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Log.d("SNACKBAR", "network connection failure");
                                }
                            });
                            snackbar.show();
                        }

                    }
                }
                else {
                    Snackbar snackbar = Snackbar.make(loginLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("SNACKBAR", "network connection failure");
                        }
                    });
                    snackbar.show();
                }

            } else {
                Snackbar snackbar = Snackbar.make(loginLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("SNACKBAR", "network connection failure");
                    }
                });
                snackbar.show();
            }
        }
    }
}
