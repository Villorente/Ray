package com.smsgt.ray;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class NetworkDetails extends AppCompatActivity {

    public boolean fromContent;
    AlertDialog dialog;

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_rays:
                    Log.d("Menu Selected", "Rays");
                    Intent intentToRays = new Intent(NetworkDetails.super.getBaseContext(), Rays.class);
                    intentToRays.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToRays);
                    finish();
                    return true;
                case R.id.navigation_devices:
                    Log.d("Menu Selected", "Devices");
                    Intent intentToDevices = new Intent(NetworkDetails.super.getBaseContext(), Devices.class);
                    intentToDevices.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDevices);
                    finish();
                    return true;
                case R.id.navigation_dashboard:
                    Log.d("Menu Selected", "Dashboard");
                    Intent intentToDashboard = new Intent(NetworkDetails.super.getBaseContext(), Dashboard.class);
                    intentToDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDashboard);
                    finish();
                    return true;
                case R.id.navigation_groups:
                    Log.d("Menu Selected", "Groups");
                    Intent intentToGroups = new Intent(NetworkDetails.super.getBaseContext(), Groups.class);
                    intentToGroups.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToGroups);
                    finish();
                    return true;
                case R.id.navigation_menu:
                    Log.d("Menu Selected", "Menu");
                    return true;
            }
            return false;
        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_network_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_chevron_left_gray_24dp);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setSelectedItemId(R.id.navigation_menu);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView appBarName = (TextView) findViewById(R.id.appbarname);
        appBarName.setTypeface(nunito);

        TextView nameLabel = (TextView) findViewById(R.id.name_label);
        nameLabel.setTypeface(nunito);
        TextView nameValue = (TextView) findViewById(R.id.name_value);
        nameValue.setTypeface(nunito);
        TextView passwordLabel = (TextView) findViewById(R.id.password_label);
        passwordLabel.setTypeface(nunito);
        TextView passwordValue = (TextView) findViewById(R.id.password_value);
        passwordValue.setTypeface(nunito);
        TextView externalIPAdressLabel = (TextView) findViewById(R.id.external_ip_address_label);
        externalIPAdressLabel.setTypeface(nunito);
        TextView externalIPAdressValue = (TextView) findViewById(R.id.external_ip_address_value);
        externalIPAdressValue.setTypeface(nunito);
        TextView gatewayIPAdressLabel = (TextView) findViewById(R.id.gateway_ip_address_label);
        gatewayIPAdressLabel.setTypeface(nunito);
        TextView gatewayIPAdressValue = (TextView) findViewById(R.id.gateway_ip_address_value);
        gatewayIPAdressValue.setTypeface(nunito);
        TextView networkSoftwareVersionLabel = (TextView) findViewById(R.id.network_software_version_label);
        networkSoftwareVersionLabel.setTypeface(nunito);
        TextView networkSoftwareVersionValue = (TextView) findViewById(R.id.network_software_version_value);
        networkSoftwareVersionValue.setTypeface(nunito);
        TextView networkTimeZoneLabel = (TextView) findViewById(R.id.network_time_zone_label);
        networkTimeZoneLabel.setTypeface(nunito);
        TextView networkTimeZoneValue = (TextView) findViewById(R.id.network_time_zone_value);
        networkTimeZoneValue.setTypeface(nunito);
        TextView advancedSettings = (TextView) findViewById(R.id.advanced_settings);
        advancedSettings.setTypeface(nunito);

        this.fromContent = false;

    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network_details, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d("Up Selected", "Up button was pressed");
                overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
                finish();
                return true;
            case R.id.edit_network_details:
                Log.d("Edit Selected", "Edit button was pressed");
                AlertDialog.Builder builder = new AlertDialog.Builder(NetworkDetails.this);
                View editNetworkDetailsView = getLayoutInflater().inflate(R.layout.dialog_edit_network_details, null);
                TextView dialogLabel = (TextView) editNetworkDetailsView.findViewById(R.id.dialog_label);
                final Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
                dialogLabel.setTypeface(nunito);
                TextView dialogNameLabel = (TextView) editNetworkDetailsView.findViewById(R.id.dialog_name_label);
                dialogNameLabel.setTypeface(nunito);
                EditText nameValue =  (EditText) editNetworkDetailsView.findViewById(R.id.dialog_name_value);
                nameValue.setTypeface(nunito);
                int nameLength = nameValue.getText().length();
                nameValue.setSelection(nameLength, nameLength);
                TextView dialogPasswordLabel = (TextView) editNetworkDetailsView.findViewById(R.id.dialog_password_label);
                dialogPasswordLabel.setTypeface(nunito);
                final EditText passwordValue =  (EditText) editNetworkDetailsView.findViewById(R.id.dialog_password_value);
                passwordValue.setTypeface(nunito);
                final View passwordLine = (View) editNetworkDetailsView.findViewById(R.id.dialog_password_line);
                final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
                final ViewGroup.LayoutParams layoutParams = passwordLine.getLayoutParams();
                final ImageView passwordShowOrHide = (ImageView) editNetworkDetailsView.findViewById(R.id.dialog_show_or_hide);
                nameValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            layoutParams.height = (int) (1 * scale + 0.5f);
                            passwordLine.setLayoutParams(layoutParams);
                        } else {
                            layoutParams.height = (int) (2 * scale + 0.5f);
                            passwordLine.setLayoutParams(layoutParams);
                        }
                    }
                });
                passwordShowOrHide.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layoutParams.height = (int) (2 * scale + 0.5f);
                        passwordLine.setLayoutParams(layoutParams);
                        int passwordLength = passwordValue.getText().length();
                        if(passwordValue.getInputType() == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)) {
                            passwordShowOrHide.setImageResource(R.drawable.ic_hide_gray_16dp);
                            passwordValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                            passwordValue.requestFocus();
                            passwordValue.setSelection(passwordLength, passwordLength);
                        } else {
                            passwordShowOrHide.setImageResource(R.drawable.ic_show_gray_16dp);
                            passwordValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            passwordValue.requestFocus();
                            passwordValue.setSelection(passwordLength, passwordLength);
                        }
                        passwordValue.setTypeface(nunito);
                    }
                });
                passwordValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            layoutParams.height = (int) (2 * scale + 0.5f);
                            passwordLine.setLayoutParams(layoutParams);
                        } else {
                            layoutParams.height = (int) (1 * scale + 0.5f);
                            passwordLine.setLayoutParams(layoutParams);
                        }
                    }
                });
                Button cancel = (Button) editNetworkDetailsView.findViewById(R.id.dialog_cancel);
                cancel.setTypeface(nunito);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("Dialog Message", "Cancel Button was pressed");
                        layoutParams.height = (int) (1 * scale + 0.5f);
                        passwordLine.setLayoutParams(layoutParams);
                        dialog.dismiss();
                    }
                });
                Button save = (Button) editNetworkDetailsView.findViewById(R.id.dialog_save);
                save.setTypeface(nunito);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("Dialog Message", "Save Button was pressed");
                        layoutParams.height = (int) (1 * scale + 0.5f);
                        passwordLine.setLayoutParams(layoutParams);
                        dialog.dismiss();
                    }
                });
                builder.setView(editNetworkDetailsView);
                dialog = builder.create();
                dialog.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPause () {
        super.onPause();
        if (this.fromContent == false) {
            overridePendingTransition(0, 0);
        } else if (this.fromContent == true){
            overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        }
    }

    @Override
    public void onBackPressed(){
        finish();
    }

    public void selectTimeZone(View view) {
        Log.d("Timezone", "Select timezone was pressed");
        Spinner timeZones = new Spinner(NetworkDetails.this, Spinner.MODE_DIALOG);
        
    }
}
