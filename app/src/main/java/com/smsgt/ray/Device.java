package com.smsgt.ray;

/**
 * Created by gabrielvillorente on 27/12/2017.
 */

public class Device {
    public String name;
    public String macAddress;
    public String manufacturer;

    Device (String name, String macAddress, String manufacturer) {
        this.name = name;
        this.macAddress = macAddress;
        this.manufacturer = manufacturer;
    }

    public void setName (String name) {
        this.name = name;
    }
}
