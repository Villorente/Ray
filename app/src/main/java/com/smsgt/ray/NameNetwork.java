package com.smsgt.ray;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class NameNetwork extends AppCompatActivity {
    AlertDialog dialog;
    private String ip = "116.50.170.243";
    private String port = "50262";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_network);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");

        TextView myNetwork = (TextView) findViewById(R.id.my_network);
        myNetwork.setTypeface(nunitoBold);

        EditText nameYourNetwork = (EditText) findViewById(R.id.name_your_network);
        nameYourNetwork.setTypeface(nunito);

        EditText password = (EditText) findViewById(R.id.password);
        password.setTypeface(nunito);
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    try {
                        goToNext(v);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(NameNetwork.this);
        View successfulRegistrationView = getLayoutInflater().inflate(R.layout.dialog_success, null);

        TextView time = (TextView) successfulRegistrationView.findViewById(R.id.dialog_title);
        time.setTypeface(nunitoBold);

        TextView dialogText = (TextView) successfulRegistrationView.findViewById(R.id.dialog_text);
        dialogText.setTypeface(nunito);

        builder.setView(successfulRegistrationView);
        dialog = builder.create();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        }, 250);



    }

    public void goToNext(View view) throws JSONException {
        EditText networkName = (EditText) findViewById(R.id.name_your_network);
        String networkNameString = networkName.getText().toString();
        EditText networkPassword = (EditText) findViewById(R.id.password);
        String networkPasswordString = networkPassword.getText().toString();
        LinearLayout nameNetworkLayout = (LinearLayout) findViewById(R.id.name_network_layout);
        if (networkNameString.isEmpty() || networkPasswordString.isEmpty()) {
            Snackbar snackbar = Snackbar.make(nameNetworkLayout, "Please complete network name and password", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else {
            //create Guest SSID 1
            SharedPreferences userSharedPreferences = getSharedPreferences("USER", Context.MODE_PRIVATE);
            String token = userSharedPreferences.getString("token", null);
            String mesh_id = userSharedPreferences.getString("mesh_id", null);
            if(token.isEmpty() || mesh_id.isEmpty()) {
                Intent intent = new Intent(NameNetwork.this, Login.class);
                startActivity(intent);
                finish();
            } else {
                HttpPostRequest createGuestSSIDRequest = new HttpPostRequest();
                JSONObject createGuestSSIDJSONObject = new JSONObject();
                createGuestSSIDJSONObject.put("mesh_id", mesh_id);
                createGuestSSIDJSONObject.put("name", "Ray-Guest"); //networkNameString originally default to "Ray-Guest"
                createGuestSSIDJSONObject.put("apply_to_all", "apply_to_all");
                createGuestSSIDJSONObject.put("encryption", "psk2");
                createGuestSSIDJSONObject.put("key", "myguestwifi"); //networkPasswordString originally default to "myguestwifi"
                createGuestSSIDJSONObject.put("macfilter", "disable");
                createGuestSSIDJSONObject.put("token", token);
                createGuestSSIDJSONObject.put("sel_language", "4_4");
                createGuestSSIDJSONObject.put("disabled", 1);
                createGuestSSIDJSONObject.put("isGuestwifi", 1);
                String createGuestSSIDJSONString = createGuestSSIDJSONObject.toString();
                String createGuestSSIDURLString = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/mesh_entry_add.json";
                String createGuestSSIDResponse = null;
                try {
                    createGuestSSIDResponse = createGuestSSIDRequest.execute(createGuestSSIDURLString, createGuestSSIDJSONString).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                Log.d("RESPONSE", createGuestSSIDResponse.toString());

                //Get Exit Poiint List 2
                HttpGetRequest getExitPointList = new HttpGetRequest();
                String getExitPointListURLString = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/mesh_entry_points.json?token="+token+"&mesh_id="+mesh_id;
                String getExitPointListResponseString = null;
                try {
                    getExitPointListResponseString = getExitPointList.execute(getExitPointListURLString).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                Log.d("Get Exit Point List", getExitPointListResponseString);

                //Create Guest SSID 3
                HttpPostRequest createGuestSSID3 = new HttpPostRequest();
                JSONObject createGuestSSID3JSONObject = new JSONObject();
                createGuestSSID3JSONObject.put("mesh_id", mesh_id);
                createGuestSSID3JSONObject.put("type", "nat");
                createGuestSSID3JSONObject.put("auto_detect", "auto_detect");
                JSONArray entryPointsArray = new JSONArray();
                createGuestSSID3JSONObject.put("entry_points[]", entryPointsArray);
                createGuestSSID3JSONObject.put("token", token);
                createGuestSSID3JSONObject.put("sel_language", "4_4");
                String createGuestSSID3URLString = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/mesh_exit_add.json";
                String createGuestSSID3Response = null;
                try {
                    createGuestSSID3Response = createGuestSSID3.execute(createGuestSSID3URLString, createGuestSSID3JSONObject.toString()).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                Log.d("Create Guest SSID 3", createGuestSSID3Response);

                //Add SSID 1 (Add Network)
                HttpPostRequest addSSIDRequest = new HttpPostRequest();
                JSONObject addSSIDJSONObject = new JSONObject();
                addSSIDJSONObject.put("mesh_id", mesh_id);
                addSSIDJSONObject.put("name", networkNameString);
                addSSIDJSONObject.put("apply_to_all", "apply_to_all");
                addSSIDJSONObject.put("encryption", "psk2");
                addSSIDJSONObject.put("key", networkPasswordString);
                addSSIDJSONObject.put("macfilter", "disable");
                addSSIDJSONObject.put("token", token);
                addSSIDJSONObject.put("sel_language", "4_4");
                addSSIDJSONObject.put("disabled", 0);
                addSSIDJSONObject.put("isGuestwifi", 0);
                String addSSIDJSONString = addSSIDJSONObject.toString();
                String addSSIDURLString = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/mesh_entry_add.json";
                String addSSIDResponse = null;
                try {
                    addSSIDResponse = addSSIDRequest.execute(addSSIDURLString, addSSIDJSONString).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                Log.d("Add SSID 1", addSSIDResponse);

                //Get Exit Poiint List after an Add
                HttpGetRequest getExitPointListAfterAdd = new HttpGetRequest();
                String getExitPointListAfterAddURLString = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/mesh_entry_points.json?token="+token+"&mesh_id="+mesh_id;
                String getExitPointListAfterAddResponseString = null;
                try {
                    getExitPointListAfterAddResponseString = getExitPointListAfterAdd.execute(getExitPointListAfterAddURLString).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                Log.d("GetExitPointList2", getExitPointListAfterAddResponseString);

                //Add SSID 2
                HttpPostRequest addSSID2 = new HttpPostRequest();
                JSONObject addSSID2JSONObject = new JSONObject();
                addSSID2JSONObject.put("mesh_id", mesh_id);
                addSSID2JSONObject.put("type", "nat");
                addSSID2JSONObject.put("auto_detect", "auto_detect");
                JSONArray exitPointsArray = new JSONArray();
                addSSID2JSONObject.put("entry_points[]", exitPointsArray);
                addSSID2JSONObject.put("token", token);
                addSSID2JSONObject.put("sel_language", "4_4");
                String addSSID2URLString = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/mesh_exit_add.json";
                String addSSID2Response = null;
                try {
                    addSSID2Response = addSSID2.execute(addSSID2URLString, addSSID2JSONObject.toString()).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                Log.d("Add SSID 2", addSSID2Response);


                Intent intent = new Intent(NameNetwork.this, LookForRay.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                finish();
            }
        }
    }
}
