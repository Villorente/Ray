package com.smsgt.ray;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AddGroup extends AppCompatActivity {

    public boolean fromContent;
    private ListView devicesListView;

    private ArrayList<Device> selectedDevices;
    private String groupNameValue;

    private String ip = "116.50.170.243";
    private String port = "50262";
    private String meshID = "167";
    private String meshEntryId = "138";
    private String token = "599a805d-a9cc-4c1b-9b91-4c0b0a0a0a1a";


    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_rays:
                    Log.d("Menu Selected", "Rays");
                    Intent intentToRays = new Intent(AddGroup.super.getBaseContext(), Rays.class);
                    intentToRays.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToRays);
                    finish();
                    return true;
                case R.id.navigation_devices:
                    Log.d("Menu Selected", "Devices");
                    Intent intentToDevices = new Intent(AddGroup.super.getBaseContext(), Devices.class);
                    intentToDevices.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDevices);
                    finish();
                    return true;
                case R.id.navigation_dashboard:
                    Log.d("Menu Selected", "Dashboard");
                    Intent intentToDashboard = new Intent(AddGroup.super.getBaseContext(), Dashboard.class);
                    intentToDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDashboard);
                    finish();
                    return true;
                case R.id.navigation_groups:
                    Log.d("Menu Selected", "Groups");
                    return true;
                case R.id.navigation_menu:
                    Log.d("Menu Selected", "Menu");
                    Intent intentToMenu = new Intent(AddGroup.super.getBaseContext(), com.smsgt.ray.Menu.class);
                    intentToMenu.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToMenu);
                    finish();
                    return true;
            }
            return false;
        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_chevron_left_gray_24dp);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setSelectedItemId(R.id.navigation_groups);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView appBarName = (TextView) findViewById(R.id.appbarname);
        appBarName.setTypeface(nunito);

        TextView groupNameLabel = (TextView) findViewById(R.id.groupnamelabel);
        groupNameLabel.setTypeface(nunito);

        EditText groupName = (EditText) findViewById(R.id.groupname);
        groupName.setTypeface(nunito);

        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");
        TextView chooseDevicesLabel = (TextView) findViewById(R.id.choosedeviceslabel);
        chooseDevicesLabel.setTypeface(nunitoBold);

        Button create = (Button) findViewById(R.id.create);
        create.setTypeface(nunito);

        this.fromContent = false;

        //
        SharedPreferences userSharedPreferences = getSharedPreferences("USER", Context.MODE_PRIVATE);
        meshID = userSharedPreferences.getString("mesh_id", null);
        token = userSharedPreferences.getString("token", null);
        //

        String urlString = "http://"+ip+":"+port+"/cake2/rd_cake/mesh_reports/get_devices.json?mesh_id="+meshID+"&token="+token;
        String responseString = null;
        ArrayList<Device> devices = new ArrayList<Device>();
        selectedDevices = new ArrayList<Device>();

        HttpGetRequest httpGetRequest = new HttpGetRequest();
        try {
            responseString = httpGetRequest.execute(urlString).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Log.d("RESPONSE", responseString);
        if (responseString != null) {
            try {
                JSONObject object = new JSONObject(responseString);
                Log.d("OBJECT",object.toString());
                JSONObject items = object.getJSONObject("items");
                Log.d("ITEMS",items.toString());
                JSONArray connected = items.getJSONArray("connected");
                if (connected.length() >= 1){
                    for (int i = 0; i < connected.length(); i++) {
                        JSONObject connectedDevice = connected.getJSONObject(i);
                        Device device = new Device(connectedDevice.getString("name"), connectedDevice.getString("mac"), connectedDevice.getString("vendor"));
                        devices.add(device);
                    }
                }
                JSONArray disconnected = items.getJSONArray("disconnected");
                if (disconnected.length() >= 1) {
                    for (int i = 0; i < disconnected.length(); i++) {
                        JSONObject disconnectedDevice = disconnected.getJSONObject(i);
                        Device device = new Device(disconnectedDevice.getString("name"), disconnectedDevice.getString("mac"), disconnectedDevice.getString("vendor"));
                        devices.add(device);
                    }
                }
                Log.d("DEVICES", devices.toString());
                Log.d("NUMBER_OF_DEVICES", String.valueOf(devices.size()));
                devicesListView = (ListView) findViewById(R.id.devices_list_view);
                DeviceAdapter adapter = new DeviceAdapter(this, devices);
                devicesListView.setAdapter(adapter);
                selectedDevices = adapter.getSelectedDevices();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d("Up Selected", "Up button was pressed");
                overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPause () {
        super.onPause();
        if (this.fromContent == false) {
            overridePendingTransition(0, 0);
        } else if (this.fromContent == true){
            overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
        }
    }
    @Override
    public void onResume () {
        super.onResume();
        if (this.fromContent == true) {
            overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
            this.fromContent = false;
        } else if (this.fromContent == false){
            overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
        }
    }
    @Override
    public void onBackPressed(){
        finish();
    }

    public void createGroup(View view) throws JSONException {
        EditText groupNameEditText = (EditText) findViewById(R.id.groupname);
        groupNameValue = groupNameEditText.getText().toString();
        Log.d("NAME", groupNameValue);
        Log.d("SELECTED DEVICES", String.valueOf(selectedDevices.size()));
        JSONArray deviceDataArray = new JSONArray();
        for (int i = 0; i < selectedDevices.size(); i++){
            JSONObject deviceDetails = new JSONObject();
            try {
                deviceDetails.put("devicename", selectedDevices.get(i).name);
                deviceDetails.put("devicemac", selectedDevices.get(i).macAddress);
                deviceDetails.put("mesh_entry_id", meshEntryId);
                deviceDetails.put("id", JSONObject.NULL);
                deviceDetails.put("mesh_id", meshID);
                deviceDetails.put("status", "start");

                deviceDataArray.put(deviceDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        JSONObject labelData = new JSONObject();
        labelData.put("devicedata", deviceDataArray);
        labelData.put("labelname", groupNameValue);
        JSONObject jsonData = new JSONObject();
        jsonData.put("labeldata", labelData);
        String jsonString = jsonData.toString();
        Log.d("JSON STRING", jsonString);

        String urlString = "http://"+ip+":"+port+"/cake2/rd_cake/Settings/addLabelWithDevice.json?token="+token;
        HttpPostRequest httpPostRequest = new HttpPostRequest();
        try {
            String responseString = httpPostRequest.execute(urlString, jsonString).get();
            Log.d("RESPONSE", responseString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        finish();
    }
}
