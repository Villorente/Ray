package com.smsgt.ray;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class RayNotFound extends AppCompatActivity {
    private String ip = "116.50.170.243";
    private String port = "50262";
    public static final String RAY_MAC_ADDRESS = "com.smsgt.ray.MAC";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ray_not_found);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView text = (TextView) findViewById(R.id.text);
        text.setTypeface(nunito);
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(nunitoBold);
        TextView tryAgain = (TextView) findViewById(R.id.try_again);
        TextView invalidCode = (TextView) findViewById(R.id.invalid_code);
        invalidCode.setTextColor(Color.parseColor("#323359"));


    }

    public void goToLookForRay(View view) {
        Intent intent = new Intent(RayNotFound.this, LookForRay.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        finish();
    }

    public void checkSerialNumber(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        HttpGetRequest checkSerialNumberRequest = new HttpGetRequest();
        EditText serialNumberEditText = (EditText) findViewById(R.id.serial_number);
        String serialNumberString = serialNumberEditText.getText().toString();
        String checkSerialNumberURLString = "http://"+ip+":"+port+"/cake2/rd_cake/nodes/check_unknown_node_availability.json?mac="+serialNumberString;
        String checkSerialNumberResponse = null;
        try {
            checkSerialNumberResponse = checkSerialNumberRequest.execute(checkSerialNumberURLString).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Log.d("Check Serial Number", checkSerialNumberResponse);
        try {
            JSONObject checkSerialNumberResponseObject = new JSONObject(checkSerialNumberResponse);
            boolean success = checkSerialNumberResponseObject.getBoolean("success");
            TextView invalidCode = (TextView) findViewById(R.id.invalid_code);
            if (success == false) {
                invalidCode.setTextColor(Color.parseColor("#F04F6A"));
            } else {
                invalidCode.setTextColor(Color.parseColor("#323359"));
                Log.d("CHECK Serial Number", "valid serial number");
                Intent intent = new Intent(RayNotFound.this, SetFirstRay.class);
                intent.putExtra(RAY_MAC_ADDRESS, serialNumberString);
                startActivity(intent);
                overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
