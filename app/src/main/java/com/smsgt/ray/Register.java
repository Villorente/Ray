package com.smsgt.ray;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Register extends AppCompatActivity {
    private String ip = "116.50.170.243";
    private String port = "50262";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");

        TextView welcome = (TextView) findViewById(R.id.welcome);
        welcome.setTypeface(nunitoBold);

        final TextView emailLabel = (TextView) findViewById(R.id.email_label);
        emailLabel.setTypeface(nunito);

        final EditText emailValue = (EditText) findViewById(R.id.email_value);
        emailValue.setTypeface(nunito);

        final TextView passwordLabel = (TextView) findViewById(R.id.password_label);
        passwordLabel.setTypeface(nunito);

        final EditText passwordValue = (EditText) findViewById(R.id.password_value);
        passwordValue.setTypeface(nunito);

        final TextView retypePasswordLabel = (TextView) findViewById(R.id.retype_password_label);
        retypePasswordLabel.setTypeface(nunito);

        final EditText retypePasswordValue = (EditText) findViewById(R.id.retype_password_value);
        retypePasswordValue.setTypeface(nunito);

        Button register = (Button) findViewById(R.id.register);
        register.setTypeface(nunito);

        TextView alreadyAMember = (TextView) findViewById(R.id.already_a_member);
        alreadyAMember.setTypeface(nunito);

        TextView login = (TextView) findViewById(R.id.login);
        login.setTypeface(nunito);

        TextView google = (TextView) findViewById(R.id.google);
        google.setTypeface(nunito);

        TextView facebook = (TextView) findViewById(R.id.facebook);
        facebook.setTypeface(nunito);

        emailValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    emailValue.getBackground().setColorFilter(Color.parseColor("#AB47BC"), PorterDuff.Mode.SRC_IN);
                } else {
                    emailLabel.setTextColor(Color.parseColor("#7F7F99"));
                    emailValue.getBackground().setColorFilter(Color.parseColor("#7F7F99"), PorterDuff.Mode.SRC_IN);
                }
            }
        });
        passwordValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    passwordValue.getBackground().setColorFilter(Color.parseColor("#AB47BC"), PorterDuff.Mode.SRC_IN);
                } else {
                    passwordLabel.setTextColor(Color.parseColor("#7F7F99"));
                    passwordValue.getBackground().setColorFilter(Color.parseColor("#7F7F99"), PorterDuff.Mode.SRC_IN);
                }
            }
        });
        retypePasswordValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    retypePasswordValue.getBackground().setColorFilter(Color.parseColor("#AB47BC"), PorterDuff.Mode.SRC_IN);
                } else {
                    retypePasswordLabel.setTextColor(Color.parseColor("#7F7F99"));
                    retypePasswordValue.getBackground().setColorFilter(Color.parseColor("#7F7F99"), PorterDuff.Mode.SRC_IN);
                }
            }
        });

    }

    public void goToLogin(View view) {
        Intent intent = new Intent(Register.this, Login.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed () {
        Intent intent = new Intent(Register.this, Step4.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        finish();
    }

    public static boolean isEmailValid(String emailString) {
        String expression = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(emailString);
        return matcher.matches();
    }

    public static boolean isPasswordValid(String passwordString) {
        String expression = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,32}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(passwordString);
        return matcher.matches();
    }


    public void registerUser(View view) throws JSONException {
        LinearLayout registerLayout = (LinearLayout) findViewById(R.id.register_layout);
        TextView emailLabel = (TextView) findViewById(R.id.email_label);
        EditText emailValue = (EditText) findViewById(R.id.email_value);
        String emailString = emailValue.getText().toString();
        TextView passwordLabel = (TextView) findViewById(R.id.password_label);
        EditText passwordValue = (EditText) findViewById(R.id.password_value);
        String passwordString = passwordValue.getText().toString();
        TextView retypePasswordLabel = (TextView) findViewById(R.id.retype_password_label);
        EditText retypePasswordValue = (EditText) findViewById(R.id.retype_password_value);
        String retypePasswordString = retypePasswordValue.getText().toString();
        if (emailString.isEmpty()) {
            Snackbar snackbar = Snackbar.make(registerLayout, "Please enter email", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            emailValue.requestFocus();
            emailValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            emailLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else if (!(isEmailValid(emailString))) {
            Snackbar snackbar = Snackbar.make(registerLayout, "Please enter a valid email", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            emailValue.requestFocus();
            emailValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            emailLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else if (passwordString.isEmpty()){
            Snackbar snackbar = Snackbar.make(registerLayout, "Please enter password", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            passwordValue.requestFocus();
            passwordValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            passwordLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else if (!(isPasswordValid(passwordString))) {
            Snackbar snackbar = Snackbar.make(registerLayout, "Password must be 8-32 characters with at least 1 upper and lower case letters, number & special character with no spaces", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            passwordValue.requestFocus();
            passwordValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            passwordLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else if (retypePasswordString.isEmpty()){
            Snackbar snackbar = Snackbar.make(registerLayout, "Please re-type password", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            retypePasswordValue.requestFocus();
            retypePasswordValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            retypePasswordLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else if (!(retypePasswordString.equals(passwordString))) {
            Snackbar snackbar = Snackbar.make(registerLayout, "Passwords do not match", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
            passwordValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            passwordLabel.setTextColor(Color.parseColor("#F04F6A"));
            retypePasswordValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
            retypePasswordLabel.setTextColor(Color.parseColor("#F04F6A"));
            snackbar.show();
        } else {
            String urlString = "http://" + ip + ":" + port + "/cake2/rd_cake/access_providers/add.json";
            String responseString = null;

            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put("parent_id", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                jsonData.put("language", "4_4");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                jsonData.put("active", "active");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                jsonData.put("username", emailString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                jsonData.put("password", passwordString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String jsonString = jsonData.toString();
            Log.d("JSONSTRING", jsonString);
            HttpPostRequest httpPostRequest = new HttpPostRequest();
            try {
                responseString = httpPostRequest.execute(urlString, jsonString).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            Log.d("RESPONSE", responseString);
            if ((responseString != null) && (!responseString.isEmpty())) {
                Log.d("RESPONSE", responseString);
                JSONObject object = null;
                try {
                    object = new JSONObject(responseString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (object != null) {
                    Boolean success = false;
                    try {
                        success = object.getBoolean("success");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("SUCCESS", success.toString());
                    if (success == false) {
                        Snackbar snackbar = Snackbar.make(registerLayout, "Email is already taken", Snackbar.LENGTH_LONG);
                        snackbar.getView().setBackgroundColor(Color.parseColor("#F04F6A"));
                        emailValue.getBackground().setColorFilter(Color.parseColor("#F04F6A"), PorterDuff.Mode.SRC_IN);
                        emailLabel.setTextColor(Color.parseColor("#F04F6A"));
                        snackbar.show();
                    } else {
                        //Log in to get token and mesh id
                        String urlLoginString = "http://" + ip + ":" + port + "/cake3/rd_cake/dashboard/authenticate.json";
                        String responseLoginString = null;

                        JSONObject jsonDataLogin = new JSONObject();
                        try {
                            jsonDataLogin.put("username", emailString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            jsonDataLogin.put("password", passwordString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String jsonLoginString = jsonDataLogin.toString();
                        Log.d("JSONSTRING", jsonLoginString);
                        HttpPostRequest loginToGetTokenPostRequest = new HttpPostRequest();
                        try {
                            responseLoginString = loginToGetTokenPostRequest.execute(urlLoginString, jsonLoginString).get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        if ((responseLoginString != null) && (!responseLoginString.isEmpty())) {
                            Log.d("RESPONSE", responseString);
                            JSONObject objectResponseFromLogin = new JSONObject(responseLoginString);
                            if (objectResponseFromLogin != null) {
                                Boolean successFromLogin = objectResponseFromLogin.getBoolean("success");
                                Log.d("SUCCESS", successFromLogin.toString());
                                if (successFromLogin) {
                                    JSONObject data = objectResponseFromLogin.getJSONObject("data");
                                    String token = data.getString("token");
                                    Log.d("TOKEN", token);
                                    String urlStringToCreateMeshId = "http://" + ip + ":" + port + "/cake2/rd_cake/meshes/add.json";
                                    String createdMeshIdResponse = null;
                                    HttpPostRequest createMeshIdRequest = new HttpPostRequest();
                                    JSONObject jsonCreateMeshObject = new JSONObject();
                                    jsonCreateMeshObject.put("name", emailString);
                                    jsonCreateMeshObject.put("token", token);
                                    jsonCreateMeshObject.put("user_id", 0);
                                    jsonCreateMeshObject.put("sel_language", "4_4");
                                    String jsonCreateMeshString = jsonCreateMeshObject.toString();
                                    try {
                                        createdMeshIdResponse = createMeshIdRequest.execute(urlStringToCreateMeshId, jsonCreateMeshString).get();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    } catch (ExecutionException e) {
                                        e.printStackTrace();
                                    }
                                    Log.d("RESPONSE", createdMeshIdResponse);
                                    JSONObject createdMeshIdResponseObject = new JSONObject(createdMeshIdResponse);
                                    Boolean createdMeshIdSuccess = createdMeshIdResponseObject.getBoolean("success");
                                    if (createdMeshIdSuccess) {
                                        //get request to get meshId
                                        HttpGetRequest getMeshIdRequest = new HttpGetRequest();
                                        String urlStringToGetMeshId = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/index.json?token="+token;
                                        String getMeshIdResponse = null;
                                        try {
                                            getMeshIdResponse = getMeshIdRequest.execute(urlStringToGetMeshId).get();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        } catch (ExecutionException e) {
                                            e.printStackTrace();
                                        }
                                        Log.d("RESPONSE", getMeshIdResponse);
                                        JSONObject meshIdResponseObject = new JSONObject(getMeshIdResponse);
                                        Boolean getMeshIdSuccess = meshIdResponseObject.getBoolean("success");
                                        if (getMeshIdSuccess) {
                                            JSONArray items = meshIdResponseObject.getJSONArray("items");
                                                JSONObject item = items.getJSONObject(0);
                                                String mesh_id = item.getString("id");
                                                Log.d("MESH ID", mesh_id);
                                                //
                                                SharedPreferences userSharedPreferences = getSharedPreferences("USER", Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = userSharedPreferences.edit();
                                                editor.putString("mesh_id", mesh_id);
                                                editor.putString("token", token);
                                                editor.apply();
                                                //
                                        } else {
                                            Snackbar snackbar = Snackbar.make(registerLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                                            snackbar.setAction("RETRY", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Log.d("SNACKBAR", "network connection failure");
                                                }
                                            });
                                            snackbar.show();
                                        }
                                    } else {
                                        Snackbar snackbar = Snackbar.make(registerLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                                        snackbar.setAction("RETRY", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Log.d("SNACKBAR", "network connection failure");
                                            }
                                        });
                                        snackbar.show();
                                    }


                                } else {
                                    Snackbar snackbar = Snackbar.make(registerLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                                    snackbar.setAction("RETRY", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Log.d("SNACKBAR", "network connection failure");
                                        }
                                    });
                                    snackbar.show();
                                }
                            } else {
                                Snackbar snackbar = Snackbar.make(registerLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                                snackbar.setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Log.d("SNACKBAR", "network connection failure");
                                    }
                                });
                                snackbar.show();
                            }

                        } else {
                            Snackbar snackbar = Snackbar.make(registerLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                            snackbar.setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Log.d("SNACKBAR", "network connection failure");
                                }
                            });
                            snackbar.show();
                        }
                    }
                    Intent intent = new Intent(Register.this, NameNetwork.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                    finish();

                } else {
                    Snackbar snackbar = Snackbar.make(registerLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("SNACKBAR", "network connection failure");
                        }
                    });
                    snackbar.show();
                }
            } else {
                Snackbar snackbar = Snackbar.make(registerLayout, "Check network connection", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("SNACKBAR", "network connection failure");
                    }
                });
                snackbar.show();
            }
        }
    }
}
