package com.smsgt.ray;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Step4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step4);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView text = (TextView) findViewById(R.id.text);
        text.setTypeface(nunito);
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(nunitoBold);
        Button createAccountButton = (Button) findViewById(R.id.create_account);
        createAccountButton.setTypeface(nunito);

        LinearLayout step4View = (LinearLayout) findViewById(R.id.step4_view);
        step4View.setOnTouchListener(new OnSwipeTouchListener(this.getApplicationContext()){
            @Override
            public void onSwipeRight() {
                Intent intent = new Intent(Step4.this, Step3.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Step4.this, Step3.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        finish();
    }

    public void goToRegister(View view) {
        Intent intent = new Intent(Step4.this, Register.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
        finish();
    }
}
