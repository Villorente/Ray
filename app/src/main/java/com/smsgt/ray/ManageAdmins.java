package com.smsgt.ray;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ManageAdmins extends AppCompatActivity {

    public boolean fromContent;
    AlertDialog dialog;
    private TextView emailHint;

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_rays:
                    Log.d("Menu Selected", "Rays");
                    Intent intentToRays = new Intent(ManageAdmins.super.getBaseContext(), Rays.class);
                    intentToRays.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToRays);
                    finish();
                    return true;
                case R.id.navigation_devices:
                    Log.d("Menu Selected", "Devices");
                    Intent intentToDevices = new Intent(ManageAdmins.super.getBaseContext(), Devices.class);
                    intentToDevices.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDevices);
                    finish();
                    return true;
                case R.id.navigation_dashboard:
                    Log.d("Menu Selected", "Dashboard");
                    Intent intentToDashboard = new Intent(ManageAdmins.super.getBaseContext(), Dashboard.class);
                    intentToDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDashboard);
                    finish();
                    return true;
                case R.id.navigation_groups:
                    Log.d("Menu Selected", "Groups");
                    Intent intentToGroups = new Intent(ManageAdmins.super.getBaseContext(), Groups.class);
                    intentToGroups.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToGroups);
                    finish();
                    return true;
                case R.id.navigation_menu:
                    Log.d("Menu Selected", "Menu");
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_admins);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_chevron_left_gray_24dp);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setSelectedItemId(R.id.navigation_menu);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView appBarName = (TextView) findViewById(R.id.appbarname);
        appBarName.setTypeface(nunito);

        TextView adminName1 = (TextView) findViewById(R.id.admin_name1);
        adminName1.setTypeface(nunito);
        TextView adminEmail1 = (TextView) findViewById(R.id.admin_email1);
        adminEmail1.setTypeface(nunito);

        TextView adminName2 = (TextView) findViewById(R.id.admin_name2);
        adminName2.setTypeface(nunito);
        TextView adminEmail2 = (TextView) findViewById(R.id.admin_email2);
        adminEmail2.setTypeface(nunito);

        this.fromContent = false;
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_manage_admins, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d("Up Selected", "Up button was pressed");
                overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
                finish();
                return true;
            case R.id.add_admin:
                Log.d("Add Selected", "Add button was pressed");
                AlertDialog.Builder builder = new AlertDialog.Builder(ManageAdmins.this);
                View inviteAdminView = getLayoutInflater().inflate(R.layout.dialog_invite_admin, null);
                TextView dialogLabel = (TextView) inviteAdminView.findViewById(R.id.dialog_label);
                final Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
                dialogLabel.setTypeface(nunito);
                TextView dialogNameLabel = (TextView) inviteAdminView.findViewById(R.id.dialog_name_label);
                dialogNameLabel.setTypeface(nunito);
                EditText nameValue =  (EditText) inviteAdminView.findViewById(R.id.dialog_name_value);
                nameValue.setTypeface(nunito);
                int nameLength = nameValue.getText().length();
                nameValue.setSelection(nameLength, nameLength);
                TextView dialogEmailLabel = (TextView) inviteAdminView.findViewById(R.id.dialog_email_label);
                dialogEmailLabel.setTypeface(nunito);
                EditText emailValue =  (EditText) inviteAdminView.findViewById(R.id.dialog_email_value);
                emailValue.setTypeface(nunito);
                //Email Hint for email verification
                emailHint = (TextView) inviteAdminView.findViewById(R.id.email_hint);
                emailHint.setTypeface(nunito);
                emailHint.setText("");
                emailValue.addTextChangedListener(new EmailTextWatcher(emailValue));
                Button cancel = (Button) inviteAdminView.findViewById(R.id.dialog_cancel);
                cancel.setTypeface(nunito);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("Dialog Message", "Cancel Button was pressed");
                        dialog.dismiss();
                    }
                });
                Button save = (Button) inviteAdminView.findViewById(R.id.dialog_save);
                save.setTypeface(nunito);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("Dialog Message", "Save Button was pressed");
                        dialog.dismiss();
                    }
                });
                builder.setView(inviteAdminView);
                dialog = builder.create();
                dialog.show();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPause () {
        super.onPause();
        if (this.fromContent == false) {
            overridePendingTransition(0, 0);
        } else if (this.fromContent == true){
            overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        }
    }
    @Override
    public void onBackPressed(){
        finish();
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private class EmailTextWatcher implements TextWatcher {
        private View view;
        private EmailTextWatcher(View view) {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            EditText emailValue = (EditText) view;
            String email = emailValue.getText().toString().trim();
            if (email.isEmpty() || !isValidEmail(email)) {
                emailHint.setText("Enter a valid email address");
            } else {
                emailHint.setText("");
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            EditText emailValue = (EditText) view;
            String email = emailValue.getText().toString().trim();
            if (email.isEmpty() || !isValidEmail(email)) {
                emailHint.setText("Enter a valid email address");
            } else {
                emailHint.setText("");
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            EditText emailValue = (EditText) view;
            String email = emailValue.getText().toString().trim();
            if (email.isEmpty() || !isValidEmail(email)) {
                emailHint.setText("Enter a valid email address");
            } else {
                emailHint.setText("");
            }
        }
    }
}
