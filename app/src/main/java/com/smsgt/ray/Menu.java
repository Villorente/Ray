package com.smsgt.ray;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

public class Menu extends AppCompatActivity implements GuestWiFiShareListDialogFragment.Listener {

    public boolean fromContent;
    AlertDialog dialog;

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_rays:
                    Log.d("Menu Selected", "Rays");
                    Intent intentToRays = new Intent(Menu.super.getBaseContext(), Rays.class);
                    intentToRays.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToRays);
                    finish();
                    return true;
                case R.id.navigation_devices:
                    Log.d("Menu Selected", "Devices");
                    Intent intentToDevices = new Intent(Menu.super.getBaseContext(), Devices.class);
                    intentToDevices.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDevices);
                    finish();
                    return true;
                case R.id.navigation_dashboard:
                    Log.d("Menu Selected", "Dashboard");
                    Intent intentToDashboard = new Intent(Menu.super.getBaseContext(), Dashboard.class);
                    intentToDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDashboard);
                    finish();
                    return true;
                case R.id.navigation_groups:
                    Log.d("Menu Selected", "Groups");
                    Intent intentToGroups = new Intent(Menu.super.getBaseContext(), Groups.class);
                    intentToGroups.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToGroups);
                    finish();
                    return true;
                case R.id.navigation_menu:
                    Log.d("Menu Selected", "Menu");
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setSelectedItemId(R.id.navigation_menu);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView appBarName = (TextView) findViewById(R.id.appbarname);
        appBarName.setTypeface(nunito);

        TextView internet = (TextView) findViewById(R.id.internet);
        internet.setTypeface(nunito);

        TextView settings = (TextView) findViewById(R.id.settings);
        settings.setTypeface(nunito);

        TextView guestWifi = (TextView) findViewById(R.id.guestwifi);
        guestWifi.setTypeface(nunito);

        TextView logout = (TextView) findViewById(R.id.logout);
        logout.setTypeface(nunito);

        this.fromContent = false;

        LinearLayout showEditGuestWiFiPwd = (LinearLayout) findViewById(R.id.dialog_guest_wifi);
        showEditGuestWiFiPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
                View editGuestWiFiPwdView = getLayoutInflater().inflate(R.layout.dialog_edit_guest_wifi_pwd, null);
                TextView dialogLabel = (TextView) editGuestWiFiPwdView.findViewById(R.id.dialog_label);
                final Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
                dialogLabel.setTypeface(nunito);
                TextView dialogNameLabel = (TextView) editGuestWiFiPwdView.findViewById(R.id.dialog_name_label);
                dialogNameLabel.setTypeface(nunito);
                EditText nameValue =  (EditText) editGuestWiFiPwdView.findViewById(R.id.dialog_name_value);
                nameValue.setTypeface(nunito);
                int nameLength = nameValue.getText().length();
                nameValue.setSelection(nameLength, nameLength);
                TextView dialogPasswordLabel = (TextView) editGuestWiFiPwdView.findViewById(R.id.dialog_password_label);
                dialogPasswordLabel.setTypeface(nunito);
                final EditText passwordValue =  (EditText) editGuestWiFiPwdView.findViewById(R.id.dialog_password_value);
                passwordValue.setTypeface(nunito);
                final View passwordLine = (View) editGuestWiFiPwdView.findViewById(R.id.dialog_password_line);
                final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
                final ViewGroup.LayoutParams layoutParams = passwordLine.getLayoutParams();
                final ImageView passwordShowOrHide = (ImageView) editGuestWiFiPwdView.findViewById(R.id.dialog_show_or_hide);
                nameValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            layoutParams.height = (int) (1 * scale + 0.5f);
                            passwordLine.setLayoutParams(layoutParams);
                        } else {
                            layoutParams.height = (int) (2 * scale + 0.5f);
                            passwordLine.setLayoutParams(layoutParams);
                        }
                    }
                });
                passwordShowOrHide.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layoutParams.height = (int) (2 * scale + 0.5f);
                        passwordLine.setLayoutParams(layoutParams);
                        int passwordLength = passwordValue.getText().length();
                        if(passwordValue.getInputType() == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)) {
                            passwordShowOrHide.setImageResource(R.drawable.ic_hide_gray_16dp);
                            passwordValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                            passwordValue.requestFocus();
                            passwordValue.setSelection(passwordLength, passwordLength);
                        } else {
                            passwordShowOrHide.setImageResource(R.drawable.ic_show_gray_16dp);
                            passwordValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            passwordValue.requestFocus();
                            passwordValue.setSelection(passwordLength, passwordLength);
                        }
                        passwordValue.setTypeface(nunito);
                    }
                });
                passwordValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            layoutParams.height = (int) (2 * scale + 0.5f);
                            passwordLine.setLayoutParams(layoutParams);
                        } else {
                            layoutParams.height = (int) (1 * scale + 0.5f);
                            passwordLine.setLayoutParams(layoutParams);
                        }
                    }
                });
                Button cancel = (Button) editGuestWiFiPwdView.findViewById(R.id.dialog_cancel);
                cancel.setTypeface(nunito);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("Dialog Message", "Cancel Button was pressed");
                        layoutParams.height = (int) (1 * scale + 0.5f);
                        passwordLine.setLayoutParams(layoutParams);
                        dialog.dismiss();
                    }
                });
                Button save = (Button) editGuestWiFiPwdView.findViewById(R.id.dialog_save);
                save.setTypeface(nunito);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("Dialog Message", "Save Button was pressed");
                        layoutParams.height = (int) (1 * scale + 0.5f);
                        passwordLine.setLayoutParams(layoutParams);
                        dialog.dismiss();
                    }
                });
                builder.setView(editGuestWiFiPwdView);
                dialog = builder.create();
                dialog.show();

            }
        });
        Switch guestWiFiSwitch = (Switch) findViewById(R.id.guest_wifi_switch);
        guestWiFiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    GuestWiFiShareListDialogFragment.newInstance(6).show(getSupportFragmentManager(), "dialog");
                }
            }
        });

    }
    @Override
    public void onPause () {
        super.onPause();
        if (this.fromContent == false) {
            overridePendingTransition(0, 0);
        } else if (this.fromContent == true) {
            overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
        }
    }
    @Override
    public void onResume () {
        super.onResume();
        if (this.fromContent == true) {
            overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
            this.fromContent = false;
        } else if (this.fromContent == false){
            overridePendingTransition(0, 0);
        }
    }
    @Override
    public void onBackPressed(){
        overridePendingTransition(0, 0);
        finish();
    }

    public void goToSettings(View view) {
        Log.d("Content Selected", "Settings was pressed");
        Intent intentToSettings = new Intent(this.getBaseContext(), Settings.class);
        this.fromContent = true;
        startActivity(intentToSettings);
        overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
    }

    @Override
    public void onGuestWiFiShareClicked(int position) {

    }
}
