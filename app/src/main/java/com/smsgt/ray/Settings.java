package com.smsgt.ray;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

public class Settings extends AppCompatActivity {

    public boolean fromContent;
    AlertDialog dialog;

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_rays:
                    Log.d("Menu Selected", "Rays");
                    Intent intentToRays = new Intent(Settings.super.getBaseContext(), Rays.class);
                    intentToRays.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToRays);
                    finish();
                    return true;
                case R.id.navigation_devices:
                    Log.d("Menu Selected", "Devices");
                    Intent intentToDevices = new Intent(Settings.super.getBaseContext(), Devices.class);
                    intentToDevices.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDevices);
                    finish();
                    return true;
                case R.id.navigation_dashboard:
                    Log.d("Menu Selected", "Dashboard");
                    Intent intentToDashboard = new Intent(Settings.super.getBaseContext(), Dashboard.class);
                    intentToDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDashboard);
                    finish();
                    return true;
                case R.id.navigation_groups:
                    Log.d("Menu Selected", "Groups");
                    Intent intentToGroups = new Intent(Settings.super.getBaseContext(), Groups.class);
                    intentToGroups.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToGroups);
                    finish();
                    return true;
                case R.id.navigation_menu:
                    Log.d("Menu Selected", "Menu");
                    return true;
            }
            return false;
        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_chevron_left_gray_24dp);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setSelectedItemId(R.id.navigation_menu);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView appBarName = (TextView) findViewById(R.id.appbarname);
        appBarName.setTypeface(nunito);

        TextView internet = (TextView) findViewById(R.id.networkdetails);
        internet.setTypeface(nunito);

        TextView settings = (TextView) findViewById(R.id.manageadmins);
        settings.setTypeface(nunito);

        TextView guestWifi = (TextView) findViewById(R.id.smartqueuemanagement);
        guestWifi.setTypeface(nunito);

        TextView logout = (TextView) findViewById(R.id.notifymeifanyrayisdown);
        logout.setTypeface(nunito);

        this.fromContent = false;

        //dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(Settings.this);
        View smartQueueMgtView = getLayoutInflater().inflate(R.layout.dialog_smart_queue_mgt, null);
        TextView dialogLabel = (TextView) smartQueueMgtView.findViewById(R.id.dialog_label);
        dialogLabel.setTypeface(nunito);
        TextView paragraph1 = (TextView) smartQueueMgtView.findViewById(R.id.dialog_paragraph_1);
        paragraph1.setTypeface(nunito);
        TextView paragraph2 = (TextView) smartQueueMgtView.findViewById(R.id.dialog_paragraph_2);
        paragraph2.setTypeface(nunito);
        final Switch smartQueueMgtSwitch = (Switch) findViewById(R.id.queueswitch);
        Button cancel = (Button) smartQueueMgtView.findViewById(R.id.dialog_cancel);
        cancel.setTypeface(nunito);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Dialog Message", "Cancel Button was pressed");
                smartQueueMgtSwitch.setChecked(false);
                dialog.dismiss();
            }
        });
        Button enable = (Button) smartQueueMgtView.findViewById(R.id.dialog_enable);
        enable.setTypeface(nunito);
        enable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Dialog Message", "Save Button was pressed");
                smartQueueMgtSwitch.setChecked(true);
                dialog.dismiss();
            }
        });
        builder.setView(smartQueueMgtView);
        dialog = builder.create();

        smartQueueMgtSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    dialog.show();
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d("Up Selected", "Up button was pressed");
                overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPause () {
        super.onPause();
        if (this.fromContent == false) {
            overridePendingTransition(0, 0);
        } else if (this.fromContent == true){
            overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
        }
    }
    @Override
    public void onResume () {
        super.onResume();
        if (this.fromContent == true) {
            overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
            this.fromContent = false;
        } else if (this.fromContent == false){
            overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
        }
    }
    @Override
    public void onBackPressed(){
        finish();
    }

    public void goToNetworkDetails(View view) {
        Log.d("Content Selected", "Network Details was pressed");
        Intent intentToNetworkDetails = new Intent(this.getBaseContext(), NetworkDetails.class);
        this.fromContent = true;
        startActivity(intentToNetworkDetails);
        overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
    }

    public void goToManageAdmins(View view) {
        Log.d("Content Selected", "Manage Admins was pressed");
        Intent intentToManageAdmins = new Intent(this.getBaseContext(), ManageAdmins.class);
        this.fromContent = true;
        startActivity(intentToManageAdmins);
        overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
    }

    public void showDialogSmartQueueMgt(View view) {
        dialog.show();
    }

    public void notify(View view) {
        Switch notifySwitch = (Switch) findViewById(R.id.notifyswicth);
        if (notifySwitch.isChecked() == true) {
            Log.d("Notifications", "Notifications was disabled");
            notifySwitch.setChecked(false);
        } else if (notifySwitch.isChecked() == false) {
            Log.d("Notifications", "Notifications was enabled");
            notifySwitch.setChecked(true);
        }
    }
}
