package com.smsgt.ray;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class OnSwipeTouchListener implements View.OnTouchListener {
    private GestureDetector gestureDetector;

    public OnSwipeTouchListener(Context context) {
        gestureDetector = new GestureDetector(context, new GestureListener());
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }
    public void onSwipeRight(){
    }
    public void onSwipeLeft(){
    }
    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final int swipeDistanceThreshold = 100;
        private static final int swipeVelocityThreshold = 100;
        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY){
            float distanceX = event2.getX() - event1.getX();
            float distanceY = event2.getY() - event1.getY();
            if((Math.abs(distanceX) > Math.abs(distanceY)) && (Math.abs(distanceX) > swipeDistanceThreshold) && (Math.abs(velocityX) > swipeVelocityThreshold)) {
                if(distanceX > 0) {
                    onSwipeRight();
                } else
                    onSwipeLeft();
                return true;
            }
            return false;
        }
    }
}
