package com.smsgt.ray;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by gabrielvillorente on 28/12/2017.
 */

public class DeviceAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Device> devices;
    private ArrayList<Device> selectedDevices;

    public DeviceAdapter(Context aContext, ArrayList<Device> aDevices) {
        context = aContext;
        devices = aDevices;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selectedDevices = new ArrayList<Device>();
    }
    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.select_devices_item, parent, false);

        final Device device = (Device) getItem(position);

        Typeface nunito = Typeface.createFromAsset(context.getAssets(), "fonts/Nunito-Regular.ttf");

        TextView deviceName = (TextView) rowView.findViewById(R.id.device_name);
        deviceName.setTypeface(nunito);
        deviceName.setText(device.name);
        TextView deviceManufacturer = (TextView) rowView.findViewById(R.id.device_manufacturer);
        deviceManufacturer.setTypeface(nunito);
        deviceManufacturer.setText(device.manufacturer);

        CheckBox deviceCheckBox = (CheckBox) rowView.findViewById(R.id.device_select);
        deviceCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()){
                    selectedDevices.add(device);
                } else {
                    Boolean successfulRemoval = selectedDevices.remove(device);
                    Log.d("REMOVED", String.valueOf(successfulRemoval));
                }
            }
        });
        return rowView;
    }

    public ArrayList<Device> getSelectedDevices() {
        return selectedDevices;
    }
}
