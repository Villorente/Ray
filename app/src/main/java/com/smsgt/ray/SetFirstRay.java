package com.smsgt.ray;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class SetFirstRay extends AppCompatActivity {
    private ColorStateList defaultTextColor;
    private String ip = "116.50.170.243";
    private String port = "50262";
    AlertDialog dialog;
    View configuringRayView;
    TextView time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_first_ray);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");

        TextView myFirstRayTextView = (TextView) findViewById(R.id.my_first_ray);
        myFirstRayTextView.setTypeface(nunitoBold);

        EditText setYourRayLocationEditText = (EditText) findViewById(R.id.set_your_ray_location);
        setYourRayLocationEditText.setTypeface(nunito);

        TextView officeRoomTextView = (TextView) findViewById(R.id.office_room);
        officeRoomTextView.setTypeface(nunito);

        TextView studyRoomTextView = (TextView) findViewById(R.id.study_room);
        studyRoomTextView.setTypeface(nunito);

        TextView livingRoomTextView = (TextView) findViewById(R.id.living_room);
        livingRoomTextView.setTypeface(nunito);

        TextView bedRoomTextView = (TextView) findViewById(R.id.bed_room);
        bedRoomTextView.setTypeface(nunito);

        defaultTextColor = officeRoomTextView.getTextColors();

        AlertDialog.Builder builder = new AlertDialog.Builder(SetFirstRay.this);
        configuringRayView = getLayoutInflater().inflate(R.layout.dialog_configuring_ray, null);

        time = (TextView) configuringRayView.findViewById(R.id.time);
        time.setTypeface(nunitoBold);

        TextView seconds = (TextView) configuringRayView.findViewById(R.id.seconds);
        seconds.setTypeface(nunito);

        TextView dialogText = (TextView) configuringRayView.findViewById(R.id.dialog_text);
        dialogText.setTypeface(nunito);

        builder.setView(configuringRayView);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    public void goToNext(View view) {
        Intent intentFrom = getIntent();
        String mac = intentFrom.getStringExtra(RayNotFound.RAY_MAC_ADDRESS);
        SharedPreferences userSharedPreferences = getSharedPreferences("USER", Context.MODE_PRIVATE);
        String token = userSharedPreferences.getString("token", null);
        String mesh_id = userSharedPreferences.getString("mesh_id", null);
        EditText setYourRayLocationEditText = (EditText) findViewById(R.id.set_your_ray_location);
        String name = setYourRayLocationEditText.getText().toString();
        JSONArray staticEntriesJSONArray = new JSONArray();
        JSONArray staticExitsJSONArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("rem_unknown", "rem_unknown");
            jsonObject.put("device_type", "standard");
            jsonObject.put("mesh_id", mesh_id);
            jsonObject.put("mac", mac);
            jsonObject.put("name", name);
            jsonObject.put("hardware", "gentworadio");
            jsonObject.put("static_entries", staticEntriesJSONArray);
            jsonObject.put("static_exits", staticExitsJSONArray);
            jsonObject.put("radio0_enable", "radio0_enable");
            jsonObject.put("radio0_entry", "radio0_entry");
            jsonObject.put("radio0_band", 24);
            jsonObject.put("radio0_two_chan", 1);
            jsonObject.put("radio1_enable", "radio1_enable");
            jsonObject.put("radio1_mesh", "radio1_mesh");
            jsonObject.put("radio1_band", 5);
            jsonObject.put("radio0_htmode", "HT20");
            jsonObject.put("radio0_disable_b", "radio0_disable_b");
            jsonObject.put("radio0_diversity", "radio0_diversity");
            jsonObject.put("radio0_ldpc", "radio0_ldpc");
            jsonObject.put("radio0_txpower", 18);
            jsonObject.put("radio0_beacon_int", 100);
            jsonObject.put("radio0_distance", 300);
            jsonObject.put("radio0_ht_capab", "SHORT-GI-40\\nRX-STBC1\\nTX-STBC\\nDSSS_CCK-40");
            jsonObject.put("radio1_htmode", "VHT80");
            jsonObject.put("radio1_diversity", "radio1_diversity");
            jsonObject.put("radio1_ldpc", "radio1_ldpc");
            jsonObject.put("radio1_txpower", 18);
            jsonObject.put("radio1_beacon_int", 100);
            jsonObject.put("radio1_distance", 300);
            jsonObject.put("radio1_ht_capab", "SHORT-GI-40\\nRX-STBC1\\nTX-STBC\\nDSSS_CCK-40");
            jsonObject.put("token", token);
            jsonObject.put("sel_language", "4_4");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String formData = jsonObject.toString();
        String urlString = "http://"+ip+":"+port+"/cake2/rd_cake/meshes/mesh_node_add.json";
        HttpPostRequest request = new HttpPostRequest();
        String response = null;
        try {
            response = request.execute(urlString, formData).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Log.d("SET UP FIRST RAY", response);

        dialog.show();

        new CountDownTimer(21000, 1000){
            @Override
            public void onTick(long millisUntilFinished) {
                String timeLeft = String.valueOf(millisUntilFinished/1000);
                time.setText(timeLeft);
            }

            @Override
            public void onFinish() {
                dialog.dismiss();
                Intent intent = new Intent(SetFirstRay.this, Rays.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                finish();
            }
        }.start();

    }

    public void setToOfficeRoom(View view) {
        TextView officeRoomTextView = (TextView) findViewById(R.id.office_room);
        officeRoomTextView.setTextColor(Color.parseColor("#FFFFFF"));
        LinearLayout officeRoomLayout = (LinearLayout) findViewById(R.id.office_room_layout);
        officeRoomLayout.setBackgroundColor(Color.parseColor("#AB47BC"));
        EditText setYourRayLocationEditText = (EditText) findViewById(R.id.set_your_ray_location);
        setYourRayLocationEditText.setText("Office Room");
        setYourRayLocationEditText.setSelection(setYourRayLocationEditText.getText().length());
        TextView studyRoomTextView = (TextView) findViewById(R.id.study_room);
        studyRoomTextView.setTextColor(defaultTextColor);
        LinearLayout studyRoomLayout = (LinearLayout) findViewById(R.id.study_room_layout);
        studyRoomLayout.setBackgroundColor(Color.TRANSPARENT);
        TextView livingRoomTextView = (TextView) findViewById(R.id.living_room);
        livingRoomTextView.setTextColor(defaultTextColor);
        LinearLayout livingRoomLayout = (LinearLayout) findViewById(R.id.living_room_layout);
        livingRoomLayout.setBackgroundColor(Color.TRANSPARENT);
        TextView bedRoomTextView = (TextView) findViewById(R.id.bed_room);
        bedRoomTextView.setTextColor(defaultTextColor);
        LinearLayout bedRoomLayout = (LinearLayout) findViewById(R.id.bed_room_layout);
        bedRoomLayout.setBackgroundColor(Color.TRANSPARENT);
    }

    public void setToStudyRoom(View view) {
        TextView studyRoomTextView = (TextView) findViewById(R.id.study_room);
        studyRoomTextView.setTextColor(Color.parseColor("#FFFFFF"));
        LinearLayout studyRoomLayout = (LinearLayout) findViewById(R.id.study_room_layout);
        studyRoomLayout.setBackgroundColor(Color.parseColor("#AB47BC"));
        EditText setYourRayLocationEditText = (EditText) findViewById(R.id.set_your_ray_location);
        setYourRayLocationEditText.setText("Study Room");
        setYourRayLocationEditText.setSelection(setYourRayLocationEditText.getText().length());
        TextView officeRoomTextView = (TextView) findViewById(R.id.office_room);
        officeRoomTextView.setTextColor(defaultTextColor);
        LinearLayout officeRoomLayout = (LinearLayout) findViewById(R.id.office_room_layout);
        officeRoomLayout.setBackgroundColor(Color.TRANSPARENT);
        TextView livingRoomTextView = (TextView) findViewById(R.id.living_room);
        livingRoomTextView.setTextColor(defaultTextColor);
        LinearLayout livingRoomLayout = (LinearLayout) findViewById(R.id.living_room_layout);
        livingRoomLayout.setBackgroundColor(Color.TRANSPARENT);
        TextView bedRoomTextView = (TextView) findViewById(R.id.bed_room);
        bedRoomTextView.setTextColor(defaultTextColor);
        LinearLayout bedRoomLayout = (LinearLayout) findViewById(R.id.bed_room_layout);
        bedRoomLayout.setBackgroundColor(Color.TRANSPARENT);
    }

    public void setToLivingRoom(View view) {
        TextView livingRoomTextView = (TextView) findViewById(R.id.living_room);
        livingRoomTextView.setTextColor(Color.parseColor("#FFFFFF"));
        LinearLayout livingRoomLayout = (LinearLayout) findViewById(R.id.living_room_layout);
        livingRoomLayout.setBackgroundColor(Color.parseColor("#AB47BC"));
        EditText setYourRayLocationEditText = (EditText) findViewById(R.id.set_your_ray_location);
        setYourRayLocationEditText.setText("Living Room");
        setYourRayLocationEditText.setSelection(setYourRayLocationEditText.getText().length());
        TextView officeRoomTextView = (TextView) findViewById(R.id.office_room);
        officeRoomTextView.setTextColor(defaultTextColor);
        LinearLayout officeRoomLayout = (LinearLayout) findViewById(R.id.office_room_layout);
        officeRoomLayout.setBackgroundColor(Color.TRANSPARENT);
        TextView studyRoomTextView = (TextView) findViewById(R.id.study_room);
        studyRoomTextView.setTextColor(defaultTextColor);
        LinearLayout studyRoomLayout = (LinearLayout) findViewById(R.id.study_room_layout);
        studyRoomLayout.setBackgroundColor(Color.TRANSPARENT);
        TextView bedRoomTextView = (TextView) findViewById(R.id.bed_room);
        bedRoomTextView.setTextColor(defaultTextColor);
        LinearLayout bedRoomLayout = (LinearLayout) findViewById(R.id.bed_room_layout);
        bedRoomLayout.setBackgroundColor(Color.TRANSPARENT);
    }

    public void setToBedRoom(View view) {
        TextView bedRoomTextView = (TextView) findViewById(R.id.bed_room);
        bedRoomTextView.setTextColor(Color.parseColor("#FFFFFF"));
        LinearLayout bedRoomLayout = (LinearLayout) findViewById(R.id.bed_room_layout);
        bedRoomLayout.setBackgroundColor(Color.parseColor("#AB47BC"));
        EditText setYourRayLocationEditText = (EditText) findViewById(R.id.set_your_ray_location);
        setYourRayLocationEditText.setText("Bed Room");
        setYourRayLocationEditText.setSelection(setYourRayLocationEditText.getText().length());
        TextView officeRoomTextView = (TextView) findViewById(R.id.office_room);
        officeRoomTextView.setTextColor(defaultTextColor);
        LinearLayout officeRoomLayout = (LinearLayout) findViewById(R.id.office_room_layout);
        officeRoomLayout.setBackgroundColor(Color.TRANSPARENT);
        TextView studyRoomTextView = (TextView) findViewById(R.id.study_room);
        studyRoomTextView.setTextColor(defaultTextColor);
        LinearLayout studyRoomLayout = (LinearLayout) findViewById(R.id.study_room_layout);
        studyRoomLayout.setBackgroundColor(Color.TRANSPARENT);
        TextView livingRoomTextView = (TextView) findViewById(R.id.living_room);
        livingRoomTextView.setTextColor(defaultTextColor);
        LinearLayout livingRoomLayout = (LinearLayout) findViewById(R.id.living_room_layout);
        livingRoomLayout.setBackgroundColor(Color.TRANSPARENT);
    }
}
