package com.smsgt.ray;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class LookForRay extends AppCompatActivity {
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_for_ray);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView text = (TextView) findViewById(R.id.text);
        text.setTypeface(nunito);
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(nunitoBold);

        AlertDialog.Builder builder = new AlertDialog.Builder(LookForRay.this);
        View lookingForRayView = getLayoutInflater().inflate(R.layout.dialog_looking_for_ray, null);

        final TextView time = (TextView) lookingForRayView.findViewById(R.id.time);
        time.setTypeface(nunitoBold);

        TextView seconds = (TextView) lookingForRayView.findViewById(R.id.seconds);
        seconds.setTypeface(nunito);

        TextView dialogText = (TextView) lookingForRayView.findViewById(R.id.dialog_text);
        dialogText.setTypeface(nunito);

        builder.setView(lookingForRayView);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        }, 1000);

        new CountDownTimer(22000, 1000){
            @Override
            public void onTick(long millisUntilFinished) {
                String timeLeft = String.valueOf(millisUntilFinished/1000);
                time.setText(timeLeft);
            }

            @Override
            public void onFinish() {
                dialog.dismiss();
                Intent intent = new Intent(LookForRay.this, RayNotFound.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                finish();
            }
        }.start();
    }
}
