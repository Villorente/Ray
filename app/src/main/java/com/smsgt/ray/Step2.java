package com.smsgt.ray;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Step2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step2);

        int currentOrientation = this.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView text = (TextView) findViewById(R.id.text);
        text.setTypeface(nunito);
        Typeface nunitoBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Bold.ttf");
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(nunitoBold);

        LinearLayout step2View = (LinearLayout) findViewById(R.id.step2_view);
        step2View.setOnTouchListener(new OnSwipeTouchListener(this.getApplicationContext()){
            @Override
            public void onSwipeLeft() {
                Intent intent = new Intent(Step2.this, Step3.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                finish();
            }
            @Override
            public void onSwipeRight() {
                Intent intent = new Intent(Step2.this, Step1.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Step2.this, Step1.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
        finish();
    }
}
