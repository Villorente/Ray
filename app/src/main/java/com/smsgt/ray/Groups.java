package com.smsgt.ray;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;

public class Groups extends AppCompatActivity {

    public boolean fromContent;

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_rays:
                    Log.d("Menu Selected", "Rays");
                    Intent intentToRays = new Intent(Groups.super.getBaseContext(), Rays.class);
                    intentToRays.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToRays);
                    finish();
                    return true;
                case R.id.navigation_devices:
                    Log.d("Menu Selected", "Devices");
                    Intent intentToDevices = new Intent(Groups.super.getBaseContext(), Devices.class);
                    intentToDevices.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDevices);
                    finish();
                    return true;
                case R.id.navigation_dashboard:
                    Log.d("Menu Selected", "Dashboard");
                    Intent intentToDashboard = new Intent(Groups.super.getBaseContext(), Dashboard.class);
                    intentToDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDashboard);
                    finish();
                    return true;
                case R.id.navigation_groups:
                    Log.d("Menu Selected", "Groups");
                    return true;
                case R.id.navigation_menu:
                    Log.d("Menu Selected", "Menu");
                    Intent intentToMenu = new Intent(Groups.super.getBaseContext(), com.smsgt.ray.Menu.class);
                    intentToMenu.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToMenu);
                    finish();
                    return true;
            }
            return false;
        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setSelectedItemId(R.id.navigation_groups);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView appBarName = (TextView) findViewById(R.id.appbarname);
        appBarName.setTypeface(nunito);

        TextView defaultText = (TextView) findViewById(R.id.default_text);
        defaultText.setTypeface(nunito);

        Typeface nunitoLight = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Light.ttf");
        TextView appBarDate = (TextView) findViewById(R.id.appbardate);
        appBarDate.setTypeface(nunitoLight);

        Date date = new Date();
        String dateToday = DateFormat.getDateInstance().format(date);
        appBarDate.setText("Today, "+dateToday);

        this.fromContent = false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_groups, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_groups:
                Log.d("AppBar", "Filter Groups was pressed");
                return true;
            case R.id.add_group:
                Log.d("AppBar", "Add Group was pressed");
                Intent intentToAddGroup = new Intent(this.getApplicationContext(), AddGroup.class);
                this.fromContent = true;
                startActivity(intentToAddGroup);
                overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    public void onPause () {
        super.onPause();
        if (this.fromContent == false) {
            overridePendingTransition(0, 0);
        } else if (this.fromContent == true) {
            overridePendingTransition(R.anim.right_to_center, R.anim.center_to_left);
        }
    }
    @Override
    public void onResume () {
        super.onResume();
        if (this.fromContent == true) {
            overridePendingTransition(R.anim.left_to_center, R.anim.center_to_right);
            this.fromContent = false;
        } else if (this.fromContent == false){
            overridePendingTransition(0, 0);
        }
    }
    @Override
    public void onBackPressed(){
        overridePendingTransition(0, 0);
        finish();
    }
}
