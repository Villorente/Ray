package com.smsgt.ray;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * <p>A fragment that shows a list of items as a modal bottom sheet.</p>
 * <p>You can show this modal bottom sheet from your activity like this:</p>
 * <pre>
 *     GuestWiFiShareListDialogFragment.newInstance(30).show(getSupportFragmentManager(), "dialog");
 * </pre>
 * <p>You activity (or fragment) needs to implement {@link GuestWiFiShareListDialogFragment.Listener}.</p>
 */
public class GuestWiFiShareListDialogFragment extends BottomSheetDialogFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_ITEM_COUNT = "item_count";
    private Listener mListener;

    // TODO: Customize parameters
    public static GuestWiFiShareListDialogFragment newInstance(int itemCount) {
        final GuestWiFiShareListDialogFragment fragment = new GuestWiFiShareListDialogFragment();
        final Bundle args = new Bundle();
        args.putInt(ARG_ITEM_COUNT, itemCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_guestwifishare_list_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(new GuestWiFiShareAdapter(getArguments().getInt(ARG_ITEM_COUNT)));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        final Fragment parent = getParentFragment();
        if (parent != null) {
            mListener = (Listener) parent;
        } else {
            mListener = (Listener) context;
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    public interface Listener {
        void onGuestWiFiShareClicked(int position);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView text;
        final ImageView image;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_guestwifishare_list_dialog_item, parent, false));
            text = (TextView) itemView.findViewById(R.id.text);
            image = (ImageView) itemView.findViewById(R.id.image);
            /*text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onGuestWiFiShareClicked(getAdapterPosition());
                        dismiss();
                    }
                }
            });*/
            LinearLayout shareItem = itemView.findViewById(R.id.share_item);
            shareItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onGuestWiFiShareClicked(getAdapterPosition());
                        dismiss();
                    }
                }
            });
        }

    }

    private class GuestWiFiShareAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final int mItemCount;

        GuestWiFiShareAdapter(int itemCount) {
            mItemCount = itemCount;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            String shareItemText;
            int shareItemImageSource = 0;
            switch (position) {
                case 0:
                    shareItemText = "Gmail";
                    shareItemImageSource = R.mipmap.logo_gmail_128px;
                    break;
                case 1:
                    shareItemText = "Hangout";
                    shareItemImageSource = R.mipmap.logo_hangouts_128px;
                    break;
                case 2:
                    shareItemText = "Google+";
                    shareItemImageSource = R.mipmap.logo_google_plus_128px;
                    break;
                case 3:
                    shareItemText = "Mail";
                    shareItemImageSource = R.drawable.ic_mail_gray_36dp;
                    break;
                case 4:
                    shareItemText = "Message";
                    shareItemImageSource = R.drawable.ic_message_gray_36dp;
                    break;
                default:
                    shareItemText = "Copy";
                    shareItemImageSource = R.drawable.ic_content_copy_gray_36dp;
                    break;
            }
            holder.text.setText(shareItemText);
            holder.image.setImageResource(shareItemImageSource);
        }

        @Override
        public int getItemCount() {
            return mItemCount;
        }

    }

}
