package com.smsgt.ray;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Rays extends AppCompatActivity {

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_rays:
                    Log.d("Menu Selected", "Rays");
                    return true;
                case R.id.navigation_devices:
                    Log.d("Menu Selected", "Devices");
                    Intent intentToDevices = new Intent(Rays.super.getBaseContext(), Devices.class);
                    intentToDevices.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDevices);
                    finish();
                    return true;
                case R.id.navigation_dashboard:
                    Log.d("Menu Selected", "Dashboard");
                    Intent intentToDashboard = new Intent(Rays.super.getBaseContext(), Dashboard.class);
                    intentToDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToDashboard);
                    finish();
                    return true;
                case R.id.navigation_groups:
                    Log.d("Menu Selected", "Groups");
                    Intent intentToGroups = new Intent(Rays.super.getBaseContext(), Groups.class);
                    intentToGroups.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToGroups);
                    finish();
                    return true;
                case R.id.navigation_menu:
                    Log.d("Menu Selected", "Menu");
                    Intent intentToMenu = new Intent(Rays.super.getBaseContext(), com.smsgt.ray.Menu.class);
                    intentToMenu.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intentToMenu);
                    finish();
                    return true;
            }
            return false;
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rays);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setSelectedItemId(R.id.navigation_rays);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Typeface nunito = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Nunito-Regular.ttf");
        TextView appBarName = (TextView) findViewById(R.id.appbarname);
        appBarName.setTypeface(nunito);
    }
    @Override
    public void onPause () {
        super.onPause();
        overridePendingTransition(0, 0);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rays, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.restart_rays:
                Log.d("AppBar", "Restart Rays was pressed");
                return true;
            case R.id.add_ray:
                Log.d("AppBar", "Add Ray was pressed");
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
